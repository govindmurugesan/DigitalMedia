 
angular
       .module('app')
       .config(config);
    	config.$inject = ['$routeProvider']

    	function config($routeProvider){
	    	   $routeProvider.
	           
	           when('/', {
	              templateUrl: 'vendorDashboard.html',
	              /*controller: 'vendorController as vendorControllerScope'*/
	           }).
	           
	           when('/login', {
	              templateUrl: 'login.html',
	              controller: 'loginController as loginControllerScope'
	           }).
	           when('/vendorfaqs', {
	              templateUrl: 'vendorfaqs.html',
	             /* controller: 'vendorfaqsxController as vendorfaqsControllerScope'*/
	           }).
	           when('/bankDetails', {
	              templateUrl: 'bankDetails.html',
	             controller: 'bankDetailsController as bankDetailsControllerScope'
	           }).
	           when('/businessDetails', {
	              templateUrl: 'businessDetails.html',
	             controller: 'businessDetailsController as businessDetailsControllerScope'
	           }).
	            when('/agencyDetails', {
	              templateUrl: 'agencyDetails.html',
	             controller: 'agencyDetailsController as agencyDetailsControllerScope'
	           }).
	           otherwise({
	              redirectTo: '/'
	           });

	          
        }
    
