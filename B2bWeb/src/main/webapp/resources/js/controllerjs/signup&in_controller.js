
angular.module("app")
.controller('signupform', ['$scope', 'RegisterService', function($scope , RegisterService) {  
  $scope.soci_sign_model = true; 
  $scope.isActive = false;
  $scope.regResponse = "";
  
  $scope.register= function(vendorUser){
	  alert(1);
	  RegisterService.VendorResister(vendorUser)
          .then(
          function(data) {
        	  $scope.regResponse = data;
        	  console.log("aaa", data);
          },
          function(errResponse){
        	  $scope.regResponse = errResponse;
              console.error('Error while fetching Users');
          }
      );
  },  
  $scope.togglesocialSignupmodel = function() {
    $scope.soci_sign_model = $scope.soci_sign_model  ===  false;
    $scope.isActive = !$scope.isActive;           
  },
  $scope.reload = function()
  {
      location.reload(); 
  },
  $scope.loginmodal= function(){
    $scope.soci_sign_model = true;
    $('#signupDesc').addClass('fade').modal('hide');
  },
  $scope.FBlogin=function(){
	  FB.login(function(response) {
		    if (response.authResponse) {
			   FB.api('/me',{fields: 'email,last_name,name,gender'}, function(response) {
			       $scope.register(response);		       
			   });
		    } else {
		    	console.log('User cancelled login or did not fully authorize.');
		    }
		}, {scope: 'email'});   
  }
 }])

.controller('loginform', ['$scope', 'RegisterService', function($scope,RegisterService) {
  $scope.soci_login_model = true;   
  $scope.isActive = false;  
  $scope.loginResponse = "";
  $scope.login  = function(user){
	  RegisterService.VendorLogin(user).then(
	      function(data) {
	    	 // $httpProvider.defaults.headers.post['token'] = data; 
	    	  console.log("login res at controler", data);	    	  
	    	  console.log("sucess save");
	    	  
	      },
	      function(errResponse){
	    	  //$httpProvider.defaults.headers.common['Auth-Token'] = '';
	    	  $scope.loginResponse = errResponse;
	          console.error('Error while fetching Users');
	      }
	   );
  }, 
  
  
  
  $scope.signInWithGoogle = function (){

      gapi.signin.render('signInButton',
      {
        'callback': signInCallback, 
        'clientid': '536532489796-7ecdc2gb172c5lleflru4ptj5ihrc0qf.apps.googleusercontent.com', 
        'requestvisibleactions': 'http://schemas.google.com/AddActivity', 
        'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email',
        'cookiepolicy': 'single_host_origin'
      });
    },
      $scope.userInfoCallback = function(userInfo) {
       console.log("userInfouserInfo",userInfo);
     },
     $scope.onLinkedInLogin=function() {
	  // pass user info to angular
	     angular.element(document.getElementById("appBody")).scope().$apply(
	    function($scope) {
	       $scope.getLinkedInData();
	    });
	  },
	 $scope.getLinkedInData = function() {
	       IN.API.Profile("me").fields(
	               [ "id", "firstName", "lastName", "pictureUrl",
	                       "publicProfileUrl" ]).result(function(result) {
	           //set the model
	           $scope.$apply(function() {
	               $scope.jsondata = result.values[0];
	           });
	       }).error(function(err) {
	           $scope.$apply(function() {
	               $scope.error = err;
	           });
	       });
	   },
 
  $scope.togglesocialloginupmodel = function() {      
    $scope.soci_login_model = $scope.soci_login_model  === false; 
    $scope.isActive = !$scope.isActive;
  },
  $scope.reload = function()
  {
      location.reload(); 
  },
  $scope.signmodal= function(){
     $scope.soci_login_model = true;
     $('#signupDesc').addClass('fade').modal('show');
     $('#loginDesc').addClass('fade').modal('hide');
  },
  $scope.resetmodal= function(){
    $scope.soci_login_model = true;
    $('#loginDesc').addClass('fade').modal('hide');
    $('#forgetpassDesc').addClass('fade').modal('show');
  },
  $scope.signInWithGoogle=function(){	  
     gapi.signin.render('signInButton',
	 {
	   'callback': $scope.signInCallback, 
	   'clientid': '536532489796-7ecdc2gb172c5lleflru4ptj5ihrc0qf.apps.googleusercontent.com', 
	   'requestvisibleactions': 'http://schemas.google.com/AddActivity', 
	   'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email',
	   'cookiepolicy': 'single_host_origin'
	 });
	},
   $scope.signInCallback = function(authResult) {
	   $scope.$apply(function() {
		   if(authResult['access_token']) {
		     $scope.signedIn = true;
		     gapi.client.request(
		     {
		       'path':'/plus/v1/people/me',
		       'method':'GET',
		       'callback': $scope.userInfoCallback
		     });
		    } else if(authResult['error']) {
		      $scope.signedIn = false;		
		   }
	   });
    };


   $scope.userInfoCallback = function(userInfo) {
      console.log("userInfouserInfo",userInfo);
    }
}])

.controller('resetform', function($scope) {
  $scope.loginmodal= function(){
    $('#forgetpassDesc').addClass('fade').modal('hide');
     $('#signupDesc').addClass('fade').modal('show');
  },
  $scope.reload = function()
  {
      location.reload(); 
  }
})

app.directive('nxEqual', function() {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, model) {
            if (!attrs.nxEqual) {
                console.error('nxEqual expects a model as an argument!');
                return;
            }
            scope.$watch(attrs.nxEqual, function (value) {
                model.$setValidity('nxEqual', value === model.$viewValue);
            });
            model.$parsers.push(function (value) {
                var isValid = value === scope.$eval(attrs.nxEqual);
                model.$setValidity('nxEqual', isValid);
                return isValid ? value : undefined;
            });
        }
    };
   
});

app.directive('phoneInput', function($filter, $browser) {
    return {
        require: 'ngModel',
        link: function($scope, $element, $attrs, ngModelCtrl) {
            var listener = function() {
                var value = $element.val().replace(/[^0-9]/g, '');
                $element.val($filter('tel')(value, false));
            };

            // This runs when we update the text field
            ngModelCtrl.$parsers.push(function(viewValue) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
            });

            // This runs when the model gets updated on the scope directly and keeps our view in sync
            ngModelCtrl.$render = function() {
                $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
            };

            $element.bind('change', listener);
            $element.bind('keydown', function(event) {
                var key = event.keyCode;
                // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                // This lets us support copy and paste too
                if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
                    return;
                }
                $browser.defer(listener); // Have to do this or changes don't get picked up properly
            });

            $element.bind('paste cut', function() {
                $browser.defer(listener);
            });
        }

    };
});
app.filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 1:
            case 2:
            case 3:
                city = value;
                break;

            default:
                city = value.slice(0, 10);
                number = value.slice(10);
        }

        if(number){
            if(number.length>0){
                number = number.slice(0, 10) + '-' + number.slice(0,10);
            }
            else{
                number = number;
            }

            return (" + city + ")  + number.trim();
        }
        else{
            return "" + city;
        }

    };
});