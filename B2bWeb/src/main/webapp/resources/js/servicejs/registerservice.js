angular.module('app')
.factory('RegisterService', RegisterService);	

RegisterService.$inject = ['$http', '$q','$localStorage'];

function RegisterService ($http, $q, $localStorage){
	
	 var REST_SERVICE_URI = 'http://localhost:8080/B2bWeb/vendor';
	 	
	    var factory = {
	    		VendorResister: VendorResister,
	    		VendorLogin: VendorLogin
	    };

	    return factory;
	    
	    function VendorResister(vendorUser) {
	        var deferred = $q.defer();
	        alert(REST_SERVICE_URI+'/register/'+vendorUser.name+'/'+vendorUser.email+'/'+vendorUser.phone+'/'+vendorUser.password)
	        
	        $http({
	            method : 'GET',
	            url :  REST_SERVICE_URI+'/register/'+vendorUser.name+'/'+vendorUser.email+'/'+vendorUser.phone+'/'+vendorUser.password,
	            transformRequest: angular.identity, 
	            transformResponse: angular.identity, 
	            headers: { 'Content-Type': undefined
	            			
	            }
	        }).then(
	            function (response) {
	                deferred.resolve(response.data);
	            },
	            function(errResponse){
	                console.log('Error while fetching Users    ', errResponse);	
	                deferred.reject(errResponse);
	            }
	        );	       
	        return deferred.promise;
	    }
	    
	    function VendorLogin(user) {
	        var deferred = $q.defer();
	        alert(REST_SERVICE_URI+'/login/'+user.name+'/'+user.password)
	        $http({
	            method : 'GET',
	            url :  REST_SERVICE_URI+'/login/'+user.name+'/'+user.password,
	            transformRequest: angular.identity, 
	            transformResponse: angular.identity, 	           
	            headers: { 'Content-Type': undefined }
	        }).then(
		            function (response) {
		            	console.log("login res", $localStorage);
		            	$localStorage.token = response.data;
		                deferred.resolve(response.data);
		            },
		            function(errResponse){
		                console.error('Error while fetching Users    ', errResponse);
		                $localStorage.token = '';
		                deferred.reject(errResponse);
		            }
		        );	       
		        return deferred.promise;
		    }
	    
	    
	    
	    
};