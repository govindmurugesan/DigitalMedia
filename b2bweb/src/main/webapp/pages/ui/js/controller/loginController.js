angular
	.module('app')
	.controller('loginController', loginController);
	
	loginController.$inject = ['$scope', 'loginService', 'registerService', '$localStorage', '$location', '$rootScope'];

	function loginController($scope, loginService, registerService, $localStorage, $location, $rootScope) {
		var loginControllerScope = this;

		loginControllerScope.login = login;
		loginControllerScope.register = register;
		loginControllerScope.forgetpass = forgetpass;
		$localStorage.jwtToken = [];

		//login method
		function login(user){
			console.log("My Login Method called ...");
			loginService.login(user).then(
	          function(data) {
				var results = JSON.parse(data);		
				var success = results.response.success;
					if(success == 'true'){
						var base64Url = $localStorage.token.split('.')[1];
						var base64 = base64Url.replace('-', '+').replace('_', '/');
						$localStorage.jwtToken = JSON.parse(atob(base64));
						console.log($localStorage.jwtToken.sub);
						$rootScope.usrName = $localStorage.jwtToken.sub
						console.log($localStorage.jwtToken)
						$rootScope.usrId = $localStorage.jwtToken.jti;
						$location.url('/bankDetails');
					}
					/* var base64Url = token.split('.')[1];
					var base64 = base64Url.replace('-', '+').replace('_', '/'); */
				  //$location.url("/bankDetails");
	          },
	          function(errResponse){
	              console.error('Error while fetching Users');
	          });			
		}

		//register method
		function register(user){
			console.log("My Register Method called ...");
			registerService.register(user).then(
			  function(data) {
				  console.log("aaa", data);
			  },
			  function(errResponse){
				  console.error('Error while fetching Users');
			  });		
		}

		//forgot password method
		function forgetpass(user){
			console.log("My forgetpass Method called ...");
			loginService.forgetpass(user).then(
	          function(data) {
	        	  console.log("aaa", data);
	          },
	          function(errResponse){
	              console.error('Error while fetching Users');
	          });
			
		}
	}
	myApp.directive("compareTo", function() {
   return {
       require: "ngModel",
       link: function(scope, element, attrs, ctrl) {

           ctrl.$validators.compareTo = function(val) {
               return val == scope.$eval(attrs.compareTo);
           };

           scope.$watch(attrs.compareTo, function() {
               ctrl.$validate();
           });
       }
   };
});