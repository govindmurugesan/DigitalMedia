(function() {
'use strict';


	var constantsService_prod = {
		url: 'http://portfolio.saptalabs.com:8080/b2bweb',
		facebookAppId : '57578686',
		googleId: '263423522351-' 
	};

	var constantsService_dev = {
		url: 'http://192.168.1.24:8080/B2bWeb',
		facebookAppId : '57578686',
		googleId: '263423522351-'

	};

	angular.module("app").constant('constantsService', constantsService_dev);

	})();
