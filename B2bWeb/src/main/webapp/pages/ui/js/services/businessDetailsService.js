angular
.module('app')
.factory('businessDetailsService', businessDetailsService);

businessDetailsService.$inject = ['$http', '$q', 'constantsService','commonService', '$localStorage', '$rootScope'];

function businessDetailsService($http, $q, constantsService, commonService, $localStorage, $rootScope) {
    var service = {
        addbusinessdetail: addbusinessdetail,
        uploadFile : uploadFile
       
    };

    return service;

    function addbusinessdetail(businessDetails){
    	console.log('You Called service Method');
        //return "DB resposne";
       /* var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url+'/login/addNewUser',
            data : request
        }).then(function(response){
            console.log('newactivity return',data);
            deferred.resolve(response);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;
    }*/
	console.log("token",$localStorage.token);
    var requestParam = {
			vendorid : $localStorage.jwtToken.jti,
            businessname : businessDetails.businessName,
			businesstype : (businessDetails.companyType == undefined ? 'proprietor': businessDetails.companyType),
            businesspan : (businessDetails.businessPAN == undefined ? '':businessDetails.businessPAN),
            personalpan : businessDetails.personalPAN,
            tinvat : businessDetails.businessTIN,
			tan : businessDetails.businessTAN
        }
	console.log(requestParam);
    var deferred = $q.defer();
	console.log($localStorage.jwtToken.jti+'   =====   '+constantsService.url+ commonService.BUSINESS_SERVICE + commonService.BUSINESS_SERVICE_ADD + '/'+ btoa(JSON.stringify(requestParam)))
           $http({
                method : 'GET',
                url :  constantsService.url+ commonService.BUSINESS_SERVICE + commonService.BUSINESS_SERVICE_ADD + '/' + btoa(JSON.stringify(requestParam)),
				transformRequest: angular.identity, 
				transformResponse: angular.identity,
				headers: { 	'Content-Type': undefined}
            }).success(function (data, status, headers, config) {
				console.log("login res", $localStorage);
				//$localStorage.token = response.data;
				deferred.resolve(data);
			}).error(function (data, status) {
				console.error('Error while fetching Users    ', data);
				//$localStorage.token = '';
				deferred.reject(data);
			});        
            return deferred.promise;
    }
    
    
    function uploadFile (file, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
    };
    
   
    }