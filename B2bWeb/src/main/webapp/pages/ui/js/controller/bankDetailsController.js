angular
	.module('app')
	.controller('bankDetailsController', bankDetailsController);

	bankDetailsController.$inject = ['$scope', 'bankDetailsService', '$location', '$localStorage', '$rootScope'];

	function bankDetailsController($scope, bankDetailsService, $location, $localStorage, $rootScope) {
		var bankDetailsControllerScope = this;

		bankDetailsControllerScope.addbankdetail = addbankdetail;
		bankDetailsControllerScope.uploadFile = uploadFile;

		function addbankdetail(bankDetails){
			bankDetailsService.addbankdetail(bankDetails).then(function(data){
				var results = data;
        console.log(data);
			});

			
			// var  response = bankDetailsService.addbankdetail();
			// console.log(response);
		}
		function uploadFile(){
	    	console.log('file is  ' );
	        var file = bankDetailsControllerScope.myFile;
	      
	        console.log(file);
	        var uploadUrl = "/fileUpload";
	        bankDetailsService.uploadFile(file, uploadUrl);
    	};

	}

myApp.directive("compareTo", function() {
    return {
        require: "ngModel",
        link: function(scope, element, attrs, ctrl) {

            ctrl.$validators.compareTo = function(val) {
                return val == scope.$eval(attrs.compareTo);
            };

            scope.$watch(attrs.compareTo, function() {
                ctrl.$validate();
            });
        }
    };
});
