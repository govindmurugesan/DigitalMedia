angular
	.module('app')
	.controller('menusController', menusController);

	menusController.$inject = ['$scope', '$localStorage', '$rootScope'];

	function menusController($scope, $localStorage, $rootScope) {
		var menusControllerScope = this;
		menusControllerScope.loginShow = true;		
		menusControllerScope.usrName = $localStorage.jwtToken.sub;		
	}