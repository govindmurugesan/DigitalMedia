angular
	.module('app')
	.controller('businessDetailsController', businessDetailsController);

	businessDetailsController.$inject = ['$scope', 'businessDetailsService','$location', '$localStorage',  '$rootScope'];

	function businessDetailsController($scope, businessDetailsService, $location, $localStorage, $rootScope ) {

		var businessDetailsControllerScope = this;

		businessDetailsControllerScope.addbusinessdetail = addbusinessdetail;
		businessDetailsControllerScope.uploadFile = uploadFile;
		
		businessDetailsService.getbusinessdetail(businessDetails)
			.then(
				function(data) {
					var results = data;	
					$rootScope.spinner=false;
					console.log(data);			
				},
				function(errResponse){
					$rootScope.spinner=false;
					console.log('errResponse  ===  ',errResponse);
				}
		);
		
		/*Add businessDetails*/
		function addbusinessdetail(businessDetails){
			console.log("GM Testing",businessDetails);
			$rootScope.spinner=true;
			businessDetailsService.addbusinessdetail(businessDetails)
			.then(
				function(data) {
					var results = data;	
					$rootScope.spinner=false;
					console.log(data);			
				},
				function(errResponse){
					$rootScope.spinner=false;
					console.log('errResponse  ===  ',errResponse);
				}
			);	
		}
		
		function uploadFile(){
	    	console.log('file is  ' );
	        var file = businessDetailsControllerScope.myFile;	      
	        console.log(file);
	        var uploadUrl = "/fileUpload";
	        businessDetailsService.uploadFile(file, uploadUrl);
    	};
    

	}
