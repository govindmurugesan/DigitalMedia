angular
.module('app')
.factory('bankDetailsService', bankDetailsService);

bankDetailsService.$inject = ['$http', '$q', 'constantsService','commonService', '$localStorage', '$rootScope'];

function bankDetailsService($http, $q, constantsService, commonService, $localStorage, $rootScope) {
    var service = {
        addbankdetail: addbankdetail,
        uploadFile: uploadFile
    };

    return service;

    function addbankdetail(bankDetails){
    	console.log('You Called service Method');
        console.log("token",$localStorage.jwtToken);
        var requestParam = {
            vendorid : $localStorage.jwtToken.jti,
            accountholdername : bankDetails.accountholdername,
            accountnumber : bankDetails.accountnumber,
            ifsc : bankDetails.Ifsc,
            bankname : bankDetails.bankname,
            state : bankDetails.state,
            city : bankDetails.city,
            branch : bankDetails.branch,
            addressprooftype : bankDetails.addressprooftype
       }
        console.log(requestParam);
           var deferred = $q.defer();
           alert($localStorage.jwtToken.jti+'   =====   '+constantsService.url+'/vendorbank/addbankdetail' + '/'+ btoa(JSON.stringify(requestParam)))
           $http({
                method : commonService.METHOD_GET,
                url :  constantsService.url + commonService.BANK_SERVICE + commonService.BANK_SERVICE_ADD + '/' + btoa(JSON.stringify(requestParam)),
                transformRequest: angular.identity, 
                transformResponse: angular.identity,               
                headers: { 'Content-Type': undefined }
                /* data : vendorUser*/
            }).then(
                    function (data) {
                        console.log("login res", $localStorage);
                        // $localStorage.token = response.data;
                        deferred.resolve(bankDetails.data);
                    },
                    function(errResponse){
                        console.error('Error while fetching Users    ', errResponse);
                         $localStorage.token = '';
                        deferred.reject(errResponse);
                    }
                );         
            return deferred.promise;
    }
    
    function uploadFile (file, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
    };
   
}