(function() {
'use strict';


	var commonService = {
		METHOD_GET : 'GET',
		METHOD_POST : 'POST',

		// login service
		LOGIN_SERVICE : '/vendor',
		LOGIN_SERVICE_REGISTER : '/register',
		LOGIN_SERVICE_LOGIN : '/login',
		LOGIN_SERVICE_FORGOT_PASSWORD : '/forgetpass',

		// bank detail service
		BANK_SERVICE : '/vendorbank',
		BANK_SERVICE_ADD : '/addbankdetail',
		/*LOGIN_SERVICE_REGISTER : '/register',
		LOGIN_SERVICE_LOGIN : '/login',
		*/	

		// agency detail service
		AGENCY_SERVICE : '/vendordetail',
		AGENCY_SERVICE_ADD : '/addvendordetail'
	};	
	

	angular.module("app").constant('commonService', commonService);

	})();
