angular
.module('app')
.factory('registerService', registerService);

registerService.$inject = ['$http', '$q', 'constantsService'];

function registerService($http, $q, constantsService) {
    var service = {
        register: register,
       
    };

    return service;

    function register(vendorUser){
    	console.log('You Called service Method');
    	 var deferred = $q.defer();
	        $http({
	            method : 'GET',
	            url :  constantsService.url+'/vendor/register/'+vendorUser.name+'/'+vendorUser.email+'/'+vendorUser.mobileNo+'/'+vendorUser.password,
	            transformRequest: angular.identity, 
	            transformResponse: angular.identity, 
	            headers: { 'Content-Type': undefined				
	            }
	        }).then(
	            function (response) {
	                deferred.resolve(response.data);
	            },
	            function(errResponse){
	                console.log('Error while fetching Users    ', errResponse);	
	                deferred.reject(errResponse);
	            }
	        );	       
	        return deferred.promise;
        
        /*var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url+'/login/addNewUser',
            data : addNewUserDetails
        }).then(function(data){
            console.log('newactivity return',data);
            deferred.resolve(data);
        }, function(error){
            console.log('error',error);
            deferred.reject(error);
        });
        return deferred.promise;*/
    }
    
   
}